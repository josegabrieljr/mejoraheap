/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author Jaimes Rodriguez
 */
public class Sort<T extends Comparable<T>> {

    private Secuencia<T> vector;

    public Sort(Secuencia<T> vtc) {
        this.vector = new Secuencia(vtc.getTamanio() + 1);
        String x = "inicio";
        this.add((T) x);
        System.err.println(this.vector.get(0));
        for (int i = 0; i < vtc.getTamanio(); i++) {
            this.add(vtc.get(i));
        }
    }

    public void add(T info) {
        this.vector.insertar(info);
        siftUp();
    }

    public void siftUp() {
        int son = this.vector.getTamanio() - 1;
        while (son > 1) {
            int dad = son / 2;
            if (this.vector.get(dad).compareTo(this.vector.get(son)) <= 0) {
                return;
            } else {
                swap(dad, son);
                son = dad;
            }

        }
    }

    public void swap(int dad, int son) {
        T temp = this.vector.get(dad);
        this.vector.set(dad, this.vector.get(son));
        this.vector.set(son, temp);
    }

    public T deleteHeap() {
        T res = this.vector.get(1);
        this.vector.set(1, this.vector.get(this.vector.getTamanio() - 1));
        this.vector.setCant(this.vector.getTamanio() - 1);
        this.siftDown();
        return res;

    }

    public void siftDown() {
        int dad = 1;
        while (dad <= this.vector.getTamanio() / 2) {
            int son = dad * 2;
            if (son > this.vector.getTamanio()) {
                break;
            }
            if (son + 1 <= this.vector.getTamanio() && this.vector.get(son + 1).compareTo(this.vector.get(son)) <= 0) {
                son++;
            }
            if (this.vector.get(dad).compareTo(this.vector.get(son)) <= 0) {
                break;
            }

            swap(dad, son);
            dad = son;
        }
        
    }

    public void heapSort() {

        Secuencia x = new Secuencia(this.vector.getTamanio());
        for (int i = this.vector.getTamanio(); i > 0; i--) {
            x.insertar(this.deleteHeap());
        }
        this.vector = x;

    }

    public Secuencia<T> getVector() {
        return vector;
    }

}
