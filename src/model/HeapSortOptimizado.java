/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author Jaimes Rodriguez
 */
public class HeapSortOptimizado<T extends Comparable> {

    private Secuencia<T> mitadUno;
    private Secuencia<T> mitadDos;
    Secuencia<T> result;

    public HeapSortOptimizado(Secuencia<T> vtc) {
        int tm = (vtc.getTamanio()) / 2;
        this.mitadUno = new Secuencia(tm + 1);
        this.mitadDos = new Secuencia(tm + 1);
        T x = (T) "inicio";
        this.add(x, 1);
        this.add(x, 2);
        for (int i = 0; i < vtc.getTamanio(); i++) {
            int al = 0;
            if (i < tm) {
                al = 1;
            } else {
                al = 2;
            }
            this.add(vtc.get(i), al);
        }

    }

    public void add(T info, int dato) {
        if (dato == 1) {
            this.mitadUno.insertar(info);
            siftUpUniV(this.mitadUno);
        } else {
            this.mitadDos.insertar(info);
            siftUpUniV(this.mitadDos);
        }
    }

    public void siftUpUniV(Secuencia<T> vtc) {
        int son = 0;
        son = vtc.getTamanio() - 1;
        while (son > 1) {
            int dad = son / 2;
            if (vtc.get(dad).compareTo(vtc.get(son)) <= 0) {
                return;
            } else {
                swapUniV(vtc, dad, son);
                son = dad;
            }
        }
    }

    public void swapUniV(Secuencia<T> vtc, int dad, int son) {
        T temp;
        temp = vtc.get(dad);
        vtc.set(dad, vtc.get(son));
        vtc.set(son, temp);

    }

    public T deleteHeapUniV(Secuencia<T> vtc) {
        T res;
        res = vtc.get(1);
        vtc.set(1, vtc.get(vtc.getTamanio() - 1));
        vtc.setCant(vtc.getTamanio() - 1);
        this.siftDownUniV(vtc);
        return res;

    }

    public void siftDownUniV(Secuencia<T> vtc) {
        int dad = 1;

        while (dad <= vtc.getTamanio() / 2) {
            int son = dad * 2;
            if (son > vtc.getTamanio()) {
                break;
            }
            if (son + 1 <= vtc.getTamanio() && vtc.get(son + 1).compareTo(vtc.get(son)) <= 0) {
                son++;
            }
            if (vtc.get(dad).compareTo(vtc.get(son)) <= 0) {
                break;
            }

            swapUniV(vtc, dad, son);
            dad = son;
        }

    }

    public void intercambio(int i, int j) {
        T aux = this.result.get(i);
        this.result.set(i, this.result.get(j));
        this.result.set(j, aux);
    }

    public void heap() {
        T info1;
        T info2;
        int cont = 0;
        info1 = this.deleteHeapUniV(this.mitadUno);
        info2 = this.deleteHeapUniV(this.mitadDos);

        this.result = new Secuencia(this.mitadDos.getTamanio() + this.mitadUno.getTamanio());
        this.result.getTamanio();
        if (info1.compareTo(info2) <= 0) {
            this.result.insertar(info1);
            this.result.insertar(info2);
        } else {
            this.result.insertar(info2);
            this.result.insertar(info1);
        }
        cont++;
        cont++;
        boolean val = true;
        while (val == true) {

            info1 = this.deleteHeapUniV(this.mitadUno);
            info2 = this.deleteHeapUniV(this.mitadDos);

            if (this.result.get(cont - 1).compareTo(info1) >= 0) {
                this.result.insertar(info1);
                cont++;
                this.intercambio(cont - 2, cont - 1);
                if (this.mitadUno.getTamanio() - 1 + this.mitadDos.getTamanio() - 1 == 0) {
                    val = false;
                }
                this.result.insertar(info2);
                cont++;

                continue;
            }

            if (this.result.get(cont - 1).compareTo(info2) >= 0) {
                this.result.insertar(info2);
                cont++;
                this.intercambio(cont - 2, cont - 1);
                if (this.mitadUno.getTamanio() - 1 + this.mitadDos.getTamanio() - 1 == 0) {
                    val = false;
                }
                this.result.insertar(info1);
                cont++;
                continue;
            }
            
            
            if (info1.compareTo(info2) <= 0) {
                this.result.insertar(info1);
                cont++;
                this.result.insertar(info2);
                cont++;
            } else {
                this.result.insertar(info2);
                cont++;
                this.result.insertar(info1);
                cont++;
            }
            if (this.mitadUno.getTamanio() - 1 + this.mitadDos.getTamanio() - 1 == 0) {
                val = false;
            }
        }
        
        this.heapMejora();
    }

    public void heapMejora() {
        Secuencia<T> x = new Secuencia(this.result.getTamanio());
        x.insertar(this.result.get(0));
        x.insertar(this.result.get(1));
        for (int i = 3; i < this.result.getTamanio(); i += 2) {
            x.insertar(this.result.get(i));
        }
        for (int i = 2; i < this.result.getTamanio(); i += 2) {
            x.insertar(this.result.get(i));
        }
        this.result = x;
        

    }
//    public void modoCambio() {
//        for (int i = this.result.getTamanio() / 2; i < this.result.getTamanio(); i++) {
//            for (int j = this.result.getTamanio() / 2; j < this.result.getTamanio(); j++) {
//                if (this.result.get(j - 1).compareTo(this.result.get(j)) >= 0) {
//                    this.intercambio(j - 1, j);
//
//                }
//            }
//        }
//    }

    public Secuencia<T> getResult() {
        return result;
    }
    
}
