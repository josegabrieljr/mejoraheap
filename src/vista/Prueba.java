/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Random;
import model.HeapSortOptimizado;
import model.Sort;
import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author Jaimes Rodriguez
 */
public class Prueba {

    public static void main(String[] args) {
        Secuencia x = new Secuencia(50000);
        Random  r= new Random();
        x.insertar(3);
        for (int i = 0; i < x.getTamanio(); i++) {
            int n= r.nextInt(9);
            x.insertar(i*4+6*n);
        }     

//----------------------------------------------------------------
        long iniSinOpt = System.currentTimeMillis();
        Sort prueba = new Sort(x);
        prueba.heapSort();
        System.out.println(prueba.getVector().toString());
        long finSinOpt = System.currentTimeMillis();
        double tiempoSinOpt = (double) ((finSinOpt - iniSinOpt) / 1000);
        System.out.println("El tiempo que tardo la ejecucion de " + x.getTamanio() + " datos sin optimizar es de: " + tiempoSinOpt + " Segundos y su orden es ascendente");
//------------------------------------------------------------------   
       long iniOpt = System.currentTimeMillis();
        HeapSortOptimizado pruebaOpt = new HeapSortOptimizado(x);
        pruebaOpt.heap();
        System.out.println(pruebaOpt.getResult().toString());
        long finOpt = System.currentTimeMillis();
        double TiempoOpt = (double) ((finOpt - iniOpt) / 1000);
        System.out.println("El tiempo que tardo la ejecucion de " + x.getTamanio() + " datos con optimizada es de: " + TiempoOpt + " Segundos y su orden es Semiordenada");
    
    }
}
